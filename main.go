package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// สังเกตว่าในแต่ละ Field ใน Book Struct จะมี Tag `json:"xxx"`
// ต่อท้ายอยู่ ซึ่ง tag เหล่านี้มีไว้สำหรับให้ Go สามารถรู้ว่าควรจะเชื่อมเอา Field
// อันไหนใน JSON มาเชื่อมกับ Field ไหนใน Struct ของเรานั่นเอง

type Book struct {
	ID     string `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
}

type Category struct {
	Type int    `json:"type"`
	Name string `json:"name"`
}

var books = []Book{
	{ID: "1", Title: "Harry Potter", Author: "J. K. Rowling"},
	{ID: "2", Title: "The Lord of the Rings", Author: "J. R. R. Tolkien"},
	{ID: "3", Title: "The Wizard of Oz", Author: "L. Frank Baum"},
}

func main() {

	rusTest()
}

func rusTest() {

	// ในโค้ดนี้เราเริ่มจากการสร้าง router ขึ้นมาด้วยคำสั่ง gin.New() และเราก็ทำการลงทะเบียน Route / ด้วยคำสั่ง
	// r.GET พร้อมใส่ Handler Function และปิดท้ายด้วยการรัน Router ด้วยคำสั่ง r.Run()

	r := gin.New()

	// same as
	// config := cors.DefaultConfig()
	// config.AllowAllOrigins = true
	// router.Use(cors.New(config))
	r.Use(cors.Default())

	r.GET("/bookCats", listBooksHandler)
	r.POST("/books", createBookHandler)
	r.DELETE("/books/:id", deleteBookHandler)

	r.Run()

}

func listBooksHandler(c *gin.Context) {

	// สังเกตว่าเราสามารถใส่ Slice ไปตรงๆ ได้เลย แล้วเราก็จะสามารถคืนค่า Book List ตามที่เราต้องการใน Response ได้เลย
	var n = getAllCat()
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
	c.Header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS")

	c.JSON(http.StatusOK, n)
}

func createBookHandler(c *gin.Context) {

	// สำหรับ Handler นี้ จะมีเพิ่มเติมในส่วนของการนำ Body ของ Request มาใช้ ผ่านคำสั่ง c.ShouldBindJSON และทำการ Bind
	// ตัวข้อมูลใน Body เข้ากับตัวแปร Struct ที่เราสร้างขึ้นที่ชื่อว่า book
	// ซึ่งถ้าหากมี Validation Error ในตอนที่ Bind เราก็ต้องทำจัดการ Error
	// นั้นและส่ง 400 Bad Request พร้อมกับ Error Message กลับไปให้กับผู้ที่ใช้งาน API ของเรา
	// พอเสร็จเรียบร้อยเราก็ทำการเพิ่ม Struct Book อันใหม่ของเราเพิ่มเข้าไปใน Slice และคืนค่า
	// Struct Book อันใหม่ของเราให้กับ Client

	var book Book

	if err := c.ShouldBindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	books = append(books, book)

	c.JSON(http.StatusCreated, book)
}

func deleteBookHandler(c *gin.Context) {

	// ในครั้งนี้เราจำเป็นต้องใช้ข้อมูลเสริมอีกตัวหนึ่ง นั่นก็คือ Book ID ที่ Client ต้องการจะลบนั่นเอง
	// ซึ่งเราสามารถใช้ Feature ของ Gin ที่เป็น Path Parameters โดยการใช้ :id เข้าไปใน Route ของเรา
	// และก็ทำการเรียกใช้ผ่านคำสั่ง c.Param("id") เพื่อดึง Param เข้ามาใช้ในตัวแปรของเรา
	// หลังจากที่ได้ ID มาจาก Param มาแล้ว เราก็ทำการเขียน Logic ในการลบ Book ที่มี ID
	// ตรงกับที่ได้รับมาออกจาก Slice ของเรา และพอเสร็จเรียบร้อยก็คืน Status 204 No Content กลับไปก็เป็นอันเสร็จ

	id := c.Param("id")

	for i, a := range books {
		if a.ID == id {
			books = append(books[:i], books[i+1:]...)
			break
		}
	}

	c.Status(http.StatusNoContent)
}

func getAllCat() []Category {
	//t := Category{ID: 0, Name: "test"}
	// connect to mongo
	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		log.Fatal(err)
	}

	// select database and collection
	mongoConnection := session.DB("master").C("bookCats")

	// insert document
	//err = mongoConnection.Insert(t)

	var formatResult []bson.M
	//var result []Category
	mongoConnection.Find(bson.M{}).All(&formatResult)
	bs, err := json.Marshal(formatResult)
	if err != nil {
		panic(err)
	}

	var o []Category
	if err := json.Unmarshal(bs, &o); err != nil {
		panic(err)
	}

	if err != nil {
		log.Fatal(err)
	}

	session.Close()

	return o
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
